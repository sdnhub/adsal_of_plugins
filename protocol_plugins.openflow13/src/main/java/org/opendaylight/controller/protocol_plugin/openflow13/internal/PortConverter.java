/*
 * Copyright (c) 2013 Cisco Systems, Inc. and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.opendaylight.controller.protocol_plugin.openflow13.internal;

import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.core.NodeConnector;
import org.opendaylight.controller.sal.core.NodeConnector.NodeConnectorIDType;

import org.openflow.protocol.OFPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract class which provides the utilities for converting the Openflow port
 * number to the equivalent NodeConnector and vice versa
 *
 *
 *
 */
public abstract class PortConverter {
    private static final Logger log = LoggerFactory
            .getLogger(PortConverter.class);
    private static final int maxOFPhysicalPort = OFPort.OFPP_MAX.getValue();

    /**
     * Converts the Openflow port number to the equivalent NodeConnector.
     */
    public static NodeConnector toNodeConnector(int ofPort, Node node) {
        // Restore original OF unsigned 16 bits value for the comparison
        long unsignedOFPort = (long)ofPort;
        log.trace("Openflow port number signed: {} unsigned: {}", ofPort,
                unsignedOFPort);
        if (unsignedOFPort > maxOFPhysicalPort) {
            if (ofPort == OFPort.OFPP_LOCAL.getValue()) {
                return OF13NodeConnectorCreator.createNodeConnector(
                        NodeConnectorIDType.SWSTACK,
                        NodeConnector.SPECIALNODECONNECTORID, node);
            } else if (ofPort == OFPort.OFPP_NORMAL.getValue()) {
                return OF13NodeConnectorCreator.createNodeConnector(
                        NodeConnectorIDType.HWPATH,
                        NodeConnector.SPECIALNODECONNECTORID, node);
            } else if (ofPort == OFPort.OFPP_CONTROLLER.getValue()) {
                return OF13NodeConnectorCreator.createNodeConnector(
                        NodeConnectorIDType.CONTROLLER,
                        NodeConnector.SPECIALNODECONNECTORID, node);
            }
        }
        return OF13NodeConnectorCreator.createNodeConnector(ofPort, node);
    }

    /**
     * Converts the NodeConnector to the equivalent Openflow port number
     */
    public static int toOFPort(NodeConnector salPort) {
        log.trace("SAL Port", salPort);
        if (salPort.getType().equals(NodeConnectorIDType.SWSTACK)) {
            return OFPort.OFPP_LOCAL.getValue();
        } else if (salPort.getType().equals(NodeConnectorIDType.HWPATH)) {
            return OFPort.OFPP_NORMAL.getValue();
        } else if (salPort.getType().equals(NodeConnectorIDType.CONTROLLER)) {
            return OFPort.OFPP_CONTROLLER.getValue();
        } else if (salPort.getType().equals("OF13")) {
            return (Integer)salPort.getID();
        }
        //Invalid
        return OFPort.OFPP_ANY.getValue();
    }
}
