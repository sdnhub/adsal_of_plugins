
/*
 * Copyright (c) 2013 Cisco Systems, Inc. and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.opendaylight.controller.protocol_plugin.openflow13.internal;


import org.openflow.protocol.OFError;
import org.openflow.protocol.OFError.OFErrorType;
import org.openflow.protocol.OFError.OFHelloFailedCode;
import org.openflow.protocol.OFError.OFBadRequestCode;
import org.openflow.protocol.OFError.OFBadActionCode;
import org.openflow.protocol.OFError.OFBadInstructionCode;
import org.openflow.protocol.OFError.OFBadMatchCode;
import org.openflow.protocol.OFError.OFFlowModFailedCode;
import org.openflow.protocol.OFError.OFPortModFailedCode;
import org.openflow.protocol.OFError.OFGroupModFailedCode;
import org.openflow.protocol.OFError.OFTableModFailedCode;
import org.openflow.protocol.OFError.OFQueueOpFailedCode;
import org.openflow.protocol.OFError.OFSwitchConfigFailedCode;
import org.openflow.protocol.OFError.OFRoleRequestFailedCode;
import org.openflow.protocol.OFError.OFMeterModFailedCode;
import org.openflow.protocol.OFError.OFTableFeaturesFailedCode;

public final class Utils {

    private Utils() { //prevent instantiation
        throw new AssertionError();
    }
    static String getOFErrorString(OFError error) {
        // Handle VENDOR extension errors here

        // Handle OF1.0 errors here
        OFErrorType et = OFErrorType.values()[0xffff & error.getErrorType()];
        String errorStr = "Error : " + et.toString();
        switch (et) {
            case OFPET_HELLO_FAILED:
                OFHelloFailedCode hfc =
                    OFHelloFailedCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + hfc.toString();
                break;
            case OFPET_BAD_REQUEST:
                OFBadRequestCode brc =
                    OFBadRequestCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + brc.toString();
                break;
            case OFPET_BAD_ACTION:
                OFBadActionCode bac =
                    OFBadActionCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + bac.toString();
                break;
            case OFPET_BAD_INSTRUCTION:
                OFBadInstructionCode bic =
                    OFBadInstructionCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + bic.toString();
                break;
            case OFPET_BAD_MATCH:
                OFBadMatchCode bmc =
                    OFBadMatchCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + bmc.toString();
                break;
            case OFPET_FLOW_MOD_FAILED:
                OFFlowModFailedCode fmfc =
                    OFFlowModFailedCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + fmfc.toString();
                break;
            case OFPET_PORT_MOD_FAILED:
                OFPortModFailedCode pmfc =
                    OFPortModFailedCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + pmfc.toString();
                break;
            case OFPET_GROUP_MOD_FAILED:
                OFGroupModFailedCode gmfc =
                    OFGroupModFailedCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + gmfc.toString();
                break;
            case OFPET_TABLE_MOD_FAILED:
                OFTableModFailedCode tmfc =
                    OFTableModFailedCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + tmfc.toString();
                break;
            case OFPET_QUEUE_OP_FAILED:
                OFQueueOpFailedCode qofc =
                    OFQueueOpFailedCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + qofc.toString();
                break;
            case OFPET_SWITCH_CONFIG_FAILED:
                OFSwitchConfigFailedCode scfc =
                    OFSwitchConfigFailedCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + scfc.toString();
                break;
            case OFPET_ROLE_REQUEST_FAILED:
                OFRoleRequestFailedCode rrfc =
                    OFRoleRequestFailedCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + rrfc.toString();
                break;
            case OFPET_METER_MOD_FAILED:
                OFMeterModFailedCode mmfc =
                    OFMeterModFailedCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + mmfc.toString();
                break;
            case OFPET_TABLE_FEATURES_FAILED:
                OFTableFeaturesFailedCode tffc =
                    OFTableFeaturesFailedCode.values()[0xffff & error.getErrorCode()];
                errorStr += " " + tffc.toString();
                break;

            default:
                break;
        }
        return errorStr;
    }
}
