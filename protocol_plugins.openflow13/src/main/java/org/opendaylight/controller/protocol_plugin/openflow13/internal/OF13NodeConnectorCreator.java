package org.opendaylight.controller.protocol_plugin.openflow13.internal;

import java.util.HashSet;
import java.util.Set;

import org.opendaylight.controller.sal.core.ConstructionException;
import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.core.NodeConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class provides helper functions to create a node connector
 *
 *
 */
public abstract class OF13NodeConnectorCreator {
    protected static final Logger logger = LoggerFactory
    .getLogger(OF13NodeConnectorCreator.class);
    /**
     * Generic NodeConnector creator
     * The nodeConnector type is inferred from the node type
     *
     * @param portId
     * @param node
     * @return
     */
    public static NodeConnector createNodeConnector(Object portId, Node node) {
        if (node.getType().equals("OF13")) {
            try {
                return new NodeConnector("OF13", (Integer)portId, node);
            } catch (ConstructionException e1) {
                logger.error("",e1);
                return null;
            }
        }
        return null;
    }

    /**
     * Generic NodeConnector creator
     * The nodeConnector type is inferred from the node type
     *
     * @param portId The string representing the port id
     * @param node The network node as {@link org.opendaylight.controller.sal.core.Node Node} object
     * @return The corresponding {@link org.opendaylight.controller.sal.core.NodeConnector NodeConnector} object
     */
    public static NodeConnector createNodeConnector(String portId, Node node) {
        return NodeConnector.fromString(String.format("%s|%s@%s", node.getType(), portId, node.toString()));
    }

    /**
     * NodeConnector creator where NodeConnector type can be specified
     * Needed to create special internal node connectors (like software stack)
     *
     * @param nodeConnectorType
     * @param portId
     * @param node
     * @return
     */
    public static NodeConnector createNodeConnector(
            String nodeConnectorType, Object portId, Node node) {
        try {
            if (nodeConnectorType.equals("OF13") && (portId.getClass() == String.class)) {
                return new NodeConnector(nodeConnectorType, Integer.parseInt((String) portId), node);
            } else {
                return new NodeConnector(nodeConnectorType, portId, node);
            }
        } catch (ConstructionException e1) {
            logger.error("",e1);
            return null;
        }
    }

    public static NodeConnector createOFNodeConnector(Integer portId, Node node) {
        try {
            return new NodeConnector("OF13", portId, node);
        } catch (ConstructionException e1) {
            logger.error("",e1);
            return null;
        }
    }

    public static Set<NodeConnector> createOFNodeConnectorSet(
            Set<Integer> portIds, Node n) {
        try {
            Set<NodeConnector> nodeConnectors = new HashSet<NodeConnector>();
            for (Integer ofPortID : portIds) {
                NodeConnector p = new NodeConnector("OF13", ofPortID, n);
                nodeConnectors.add(p);
            }
            return nodeConnectors;
        } catch (ConstructionException e1) {
            logger.error("",e1);
            return null;
        }
    }
}
