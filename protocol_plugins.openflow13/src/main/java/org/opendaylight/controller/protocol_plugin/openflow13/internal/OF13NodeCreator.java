package org.opendaylight.controller.protocol_plugin.openflow13.internal;

import org.opendaylight.controller.sal.core.ConstructionException;
import org.opendaylight.controller.sal.core.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for creating a Node object
 *
 *
 *
 */
public abstract class OF13NodeCreator {
    protected static final Logger logger = LoggerFactory.getLogger(OF13NodeCreator.class);

    public static Node createOFNode(Long switchId) {
        try {
            return new Node("OF13", switchId);
        } catch (ConstructionException e1) {
            logger.error("",e1);
            return null;
        }
    }
}
