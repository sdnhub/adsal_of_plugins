package org.opendaylight.controller.protocol_plugin.openflow13.internal;

import org.opendaylight.controller.sal.core.ConstructionException;
import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.core.NodeTable;
import org.opendaylight.controller.sal.core.NodeTable.NodeTableIDType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class OF13NodeTableCreator {
    protected static final Logger logger = LoggerFactory
            .getLogger(OF13NodeTableCreator.class);

    /**
     * OF13 NodeTable creator
     *
     * @param portId
     * @param node
     * @return
     */
    public static NodeTable createNodeTable(byte tableId, Node node) {
        try {
            return new NodeTable("OF13", tableId, node);
        } catch (ConstructionException e1) {
            logger.error("",e1);
            return null;
        }
    }

    public static NodeTable createOFNodeTable(byte tableId, Node node) {
        try {
            return new NodeTable("OF13", tableId, node);
        } catch (ConstructionException e1) {
            logger.error("",e1);
            return null;
        }
    }

}
