/*
 * Copyright (c) 2013 Cisco Systems, Inc. and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.opendaylight.controller.protocol_plugin.openflow13.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.opendaylight.controller.sal.action.Action;
import org.opendaylight.controller.sal.action.ActionType;
import org.opendaylight.controller.sal.action.Controller;
import org.opendaylight.controller.sal.action.Drop;
import org.opendaylight.controller.sal.action.Enqueue;
import org.opendaylight.controller.sal.action.Flood;
import org.opendaylight.controller.sal.action.FloodAll;
import org.opendaylight.controller.sal.action.HwPath;
import org.opendaylight.controller.sal.action.Loopback;
import org.opendaylight.controller.sal.action.Output;
import org.opendaylight.controller.sal.action.PopVlan;
import org.opendaylight.controller.sal.action.PushVlan;
import org.opendaylight.controller.sal.action.SetDlType;
import org.opendaylight.controller.sal.action.SetDlDst;
import org.opendaylight.controller.sal.action.SetDlSrc;
import org.opendaylight.controller.sal.action.SetNwDst;
import org.opendaylight.controller.sal.action.SetNwSrc;
import org.opendaylight.controller.sal.action.SetNwTos;
import org.opendaylight.controller.sal.action.SetTpDst;
import org.opendaylight.controller.sal.action.SetTpSrc;
import org.opendaylight.controller.sal.action.SetVlanId;
import org.opendaylight.controller.sal.action.SetVlanPcp;
import org.opendaylight.controller.sal.action.SwPath;
import org.opendaylight.controller.sal.utils.EtherTypes;
import org.opendaylight.controller.sal.utils.IPProtocols;
import org.opendaylight.controller.sal.utils.NetUtils;
import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.core.NodeConnector;
import org.opendaylight.controller.sal.flowprogrammer.Flow;
import org.opendaylight.controller.sal.match.Match;
import org.opendaylight.controller.sal.match.MatchType;
import org.opendaylight.controller.sal.match.MatchField;
import org.openflow.protocol.OFFlowMod;
import org.openflow.protocol.OFMatch;
import org.openflow.protocol.OFMessage;
import org.openflow.protocol.OFOXMField;
import org.openflow.protocol.OFOXMFieldType;
import org.openflow.protocol.OFPacketOut;
import org.openflow.protocol.OFPort;
import org.openflow.protocol.OFQueue;
import org.openflow.protocol.OFVlanId;
import org.openflow.protocol.action.OFAction;
import org.openflow.protocol.action.OFActionPopVLAN;
import org.openflow.protocol.action.OFActionPushVLAN;
import org.openflow.protocol.action.OFActionSetField;
import org.openflow.protocol.action.OFActionOutput;
import org.openflow.protocol.action.OFActionSetQueue;
import org.openflow.protocol.instruction.OFInstruction;
import org.openflow.protocol.instruction.OFInstructionApplyActions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for converting a SAL Flow into the OF flow and vice-versa
 */
public class FlowConverter {
    protected static final Logger logger = LoggerFactory
            .getLogger(FlowConverter.class);

    private Flow flow; // SAL Flow
    private OFMatch ofMatch; // OF 1.3.3 match
    private List<OFAction> actionsList; // OF 1.3 actions
    private boolean isIPv6;

    public FlowConverter(OFMatch ofMatch, List<OFAction> actionsList) {
        this.ofMatch = ofMatch;
        this.actionsList = actionsList;
        this.flow = null;
        this.isIPv6 = false;
    }

    public FlowConverter(Flow flow) {
        this.ofMatch = null;
        this.actionsList = null;
        this.flow = flow;
        this.isIPv6 = flow.isIPv6();
    }

    /**
     * Returns the match in OF 1.3.3 (OFMatch) form
     *
     * @return
     */
    public OFMatch getOFMatch() {
        if (ofMatch == null) {
            Match match = flow.getMatch();
            short dlType;
            byte nwProto;
            //TODO: Add support for IPv6
            if (isIPv6)
                return null;

            ofMatch = new OFMatch();

            if (match.isPresent(MatchType.IN_PORT)) {
                int port = (Integer) ((NodeConnector) match.getField(
                        MatchType.IN_PORT).getValue()).getID();
                ofMatch.setInPort(port);
            }
            if (match.isPresent(MatchType.DL_SRC)) {
                byte[] srcMac = (byte[]) match.getField(MatchType.DL_SRC)
                        .getValue();
                ofMatch.setDataLayerSource(srcMac.clone());
            }
            if (match.isPresent(MatchType.DL_DST)) {
                byte[] dstMac = (byte[]) match.getField(MatchType.DL_DST)
                        .getValue();
                ofMatch.setDataLayerDestination(dstMac.clone());
            }
            if (match.isPresent(MatchType.DL_VLAN)) {
                short vlan = (Short) match.getField(MatchType.DL_VLAN)
                        .getValue();
                if (vlan != MatchType.DL_VLAN_NONE) {
                    ofMatch.setDataLayerVirtualLan(vlan);
                }
            }
            if (match.isPresent(MatchType.DL_VLAN_PR)) {
                byte vlanPr = (Byte) match.getField(MatchType.DL_VLAN_PR)
                        .getValue();
                ofMatch.setDataLayerVirtualLanPriorityCodePoint(vlanPr);
            }

            if (match.isPresent(MatchType.DL_TYPE)) {
                dlType = (Short) match.getField(MatchType.DL_TYPE)
                        .getValue();
                ofMatch.setDataLayerType(dlType);
            } else {
                dlType = EtherTypes.IPv4.shortValue();
                // Tracking pre-requisite fields
                if (match.isPresent(MatchType.NW_TOS) || match.isPresent(MatchType.NW_SRC) ||
                        match.isPresent(MatchType.NW_DST) || match.isPresent(MatchType.TP_SRC)
                        || match.isPresent(MatchType.TP_DST))
                    //Defaults to IPv4 if DL_TYPE is unspecified
                    ofMatch.setDataLayerType(dlType);
            }

            if (match.isPresent(MatchType.NW_PROTO)) {
                nwProto = (Byte) match.getField(MatchType.NW_PROTO)
                        .getValue();
                ofMatch.setNetworkProtocol(nwProto);
            } else {
                nwProto = IPProtocols.TCP.byteValue();
                // Tracking pre-requisite fields
                if (match.isPresent(MatchType.TP_SRC) || match.isPresent(MatchType.TP_DST))
                    //Defaults to TCP if NW_PROTO is unspecified
                    ofMatch.setNetworkProtocol(IPProtocols.TCP.byteValue());
            }

            if (match.isPresent(MatchType.NW_TOS)) {
                byte tos = (Byte) match.getField(MatchType.NW_TOS).getValue();
                ofMatch.setNetworkTypeOfService(tos);
            }

            if (match.isPresent(MatchType.NW_SRC)) {
                InetAddress address = (InetAddress) match.getField(MatchType.NW_SRC).getValue();
                InetAddress mask = (InetAddress) match.getField(MatchType.NW_SRC).getMask();
                ofMatch.setNetworkSourceMask(dlType, NetUtils.byteArray4ToInt(address.getAddress()),
                        (mask == null)? 0: NetUtils.byteArray4ToInt(mask.getAddress()));
            }

            if (match.isPresent(MatchType.NW_DST)) {
                InetAddress address = (InetAddress) match.getField(MatchType.NW_DST).getValue();
                InetAddress mask = (InetAddress) match.getField(MatchType.NW_DST).getMask();
                ofMatch.setNetworkDestinationMask(dlType, NetUtils.byteArray4ToInt(address.getAddress()),
                        (mask == null)? 0: NetUtils.byteArray4ToInt(mask.getAddress()));
            }
            if (match.isPresent(MatchType.TP_SRC)) {
                short port = (Short) match.getField(MatchType.TP_SRC)
                        .getValue();
                ofMatch.setTransportSource(nwProto, port);
            }
            if (match.isPresent(MatchType.TP_DST)) {
                short port = (Short) match.getField(MatchType.TP_DST)
                        .getValue();
                ofMatch.setTransportDestination(nwProto, port);
            }
        }
        logger.trace("SAL Match: {} Openflow Match: {}", flow.getMatch(),
                ofMatch);
        return ofMatch;
    }

    /**
     * Returns the list of actions in OF 1.0 form
     *
     * @return
     */
    public List<OFAction> getOFActions() {
        //One time generation of the actionList
        if (this.actionsList == null) {
            actionsList = new ArrayList<OFAction>();
            Match match = flow.getMatch();

            short dlType = EtherTypes.IPv4.shortValue();
            byte nwProto = IPProtocols.TCP.byteValue();

            //We do not use this field right now
            if (match.isPresent(MatchType.DL_TYPE)) {
                dlType = (Short) match.getField(MatchType.DL_TYPE).getValue();
            }
            //We use the nwProto to decide protocol for the set field
            if (match.isPresent(MatchType.NW_PROTO)) {
                nwProto = (Byte) match.getField(MatchType.NW_PROTO).getValue();
            }

            //Unline match fields, sequencing is very important for
            //actions. So a loop based operation is acceptable.
            for (Action action : flow.getActions()) {
                if (action.getType() == ActionType.OUTPUT) {
                    Output a = (Output) action;
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setMaxLength((short) 0xffff);
                    ofAction.setPort(PortConverter.toOFPort(a.getPort()));
                    actionsList.add(ofAction);
                    continue;
                }

                if (action.getType() == ActionType.ENQUEUE) {
                    Enqueue a = (Enqueue) action;
                    OFActionSetQueue ofAction = new OFActionSetQueue();
                    ofAction.setQueueId(a.getQueue());
                    actionsList.add(ofAction);
                    continue;
                }

                if (action.getType() == ActionType.DROP) {
                    continue;
                }
                if (action.getType() == ActionType.LOOPBACK) {
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setPort(OFPort.OFPP_IN_PORT.getValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.FLOOD) {
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setPort(OFPort.OFPP_FLOOD.getValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.FLOOD_ALL) {
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setPort(OFPort.OFPP_ALL.getValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.CONTROLLER) {
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setPort(OFPort.OFPP_CONTROLLER.getValue());
                    // We want the whole frame hitting the match be sent to the
                    // controller
                    ofAction.setMaxLength((short) 0xffff);
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SW_PATH) {
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setPort(OFPort.OFPP_LOCAL.getValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.HW_PATH) {
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setPort(OFPort.OFPP_NORMAL.getValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_VLAN_ID) {
                    SetVlanId a = (SetVlanId) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.VLAN_VID, (short)a.getVlanId());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_VLAN_PCP) {
                    SetVlanPcp a = (SetVlanPcp) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.VLAN_PCP, Integer.valueOf(
                            a.getPcp()).byteValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.POP_VLAN) {
                    OFActionPopVLAN ofAction = new OFActionPopVLAN();
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.PUSH_VLAN) {
                    PushVlan a = (PushVlan)action;
                    //Get ethertype
                    OFActionPushVLAN ofActionPushVlan = new OFActionPushVLAN((short) a.getTag());
                    actionsList.add(ofActionPushVlan);

                    //Get VLAN ID
                    if (a.getVlanId() != 0) {
                        OFActionSetField ofActionSetVlanId = new OFActionSetField(OFOXMFieldType.VLAN_VID, (short)a.getVlanId());
                        actionsList.add(ofActionSetVlanId);
                    }
                    //Get VLAN PCP
                    if (a.getPcp() != 0) {
                        OFActionSetField ofActionSetVlanPcp = new OFActionSetField(OFOXMFieldType.VLAN_PCP, Integer.valueOf(
                                a.getPcp()).byteValue());
                        actionsList.add(ofActionSetVlanPcp);
                    }
                    continue;
                }
                if (action.getType() == ActionType.SET_DL_TYPE) {
                    SetDlType a = (SetDlType) action;
                    dlType = (short)a.getDlType();
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.ETH_TYPE, (short)a.getDlType());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_DL_SRC) {
                    SetDlSrc a = (SetDlSrc) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.ETH_SRC, a.getDlAddress());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_DL_DST) {
                    SetDlDst a = (SetDlDst) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.ETH_DST, a.getDlAddress());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_NW_SRC) {
                    SetNwSrc a = (SetNwSrc) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.IPV4_SRC, a.getAddressAsString());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_NW_DST) {
                    SetNwDst a = (SetNwDst) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.IPV4_DST, a.getAddressAsString());
                    actionsList.add(ofAction);

                    continue;
                }
                if (action.getType() == ActionType.SET_NW_TOS) {
                    SetNwTos a = (SetNwTos) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.IP_DSCP, Integer.valueOf(
                            a.getNwTos()).byteValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_TP_SRC) {
                    SetTpSrc a = (SetTpSrc) action;
                    OFOXMFieldType fieldType;
                    if (nwProto == IPProtocols.TCP.byteValue())
                       fieldType = OFOXMFieldType.TCP_SRC;
                    else if (nwProto == IPProtocols.UDP.byteValue())
                       fieldType = OFOXMFieldType.UDP_SRC;
                    else if (nwProto == IPProtocols.SCTP.byteValue())
                       fieldType = OFOXMFieldType.SCTP_SRC;
                    else  //Skip
                        continue;
                    OFActionSetField ofAction = new OFActionSetField(fieldType, Integer.valueOf(a.getPort())
                            .shortValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_TP_DST) {
                    SetTpDst a = (SetTpDst) action;
                    OFOXMFieldType fieldType;
                    if (nwProto == IPProtocols.TCP.byteValue())
                       fieldType = OFOXMFieldType.TCP_DST;
                    else if (nwProto == IPProtocols.UDP.byteValue())
                       fieldType = OFOXMFieldType.UDP_DST;
                    else if (nwProto == IPProtocols.SCTP.byteValue())
                       fieldType = OFOXMFieldType.SCTP_DST;
                    else  //Skip
                        continue;
                    OFActionSetField ofAction = new OFActionSetField(fieldType, Integer.valueOf(a.getPort())
                            .shortValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_NEXT_HOP) {
                    logger.warn("Unsupported action: {}", action);
                    continue;
                }
            }
        }
        logger.trace("SAL Actions: {} Openflow Actions: {}", flow.getActions(),
                actionsList);
        return actionsList;
    }

    /**
     * Utility to convert a SAL flow to an OF 1.0 (OFFlowMod) or to an OF 1.0 +
     * IPv6 extension (V6FlowMod) Flow modifier Message
     *
     * @param sw
     * @param command
     * @param port
     * @return
     */
    public OFMessage getOFFlowMod(byte command, OFPort port) {
        OFMessage fm = new OFFlowMod();
        if (this.ofMatch == null) {
            getOFMatch();
        }
        if (this.actionsList == null) {
            getOFActions();
        }
        if (!isIPv6) {
            ((OFFlowMod) fm).setMatch(this.ofMatch);
            // In case of delete, we assume that instructions are not needed
            if ((command != OFFlowMod.OFPFC_DELETE_STRICT) && (command != OFFlowMod.OFPFC_DELETE))
                //TODO: Add support for parsing goto_table instructions
                ((OFFlowMod) fm).setInstructions(Collections.singletonList((OFInstruction) new OFInstructionApplyActions()
                .setActions(this.actionsList)));

            ((OFFlowMod) fm).setPriority(flow.getPriority());
            ((OFFlowMod) fm).setCookie(flow.getId());
            ((OFFlowMod) fm).setBufferId(OFPacketOut.BUFFER_ID_NONE);
            ((OFFlowMod) fm).setIdleTimeout(flow.getIdleTimeout());
            ((OFFlowMod) fm).setHardTimeout(flow.getHardTimeout());
            ((OFFlowMod) fm).setCommand(command);
            if (port != null) {
                ((OFFlowMod) fm).setOutPort(port);
            }
            if (command == OFFlowMod.OFPFC_ADD || command == OFFlowMod.OFPFC_MODIFY
                    || command == OFFlowMod.OFPFC_MODIFY_STRICT) {
                // Instruct switch to let controller know when flow is removed
                ((OFFlowMod) fm).setFlags((short) 1);
            }
        }
        logger.trace("Openflow Match: {} Openflow Actions: {}", ofMatch,
                actionsList);
        logger.trace("Openflow Mod Message: {}", fm);
        return fm;
    }

    public Flow getFlow(Node node) {
        if (this.flow == null) {
            Match salMatch = new Match();
            List<Action> salActionList = new ArrayList<Action>();

            /*
             * Installed flow may not have a Match defined like in case of a
             * drop all flow
             */
            if (ofMatch != null) {
                if (!isIPv6) {
                    if (ofMatch.getInPort() != OFPort.OFPP_ANY.getValue() &&
                        ofMatch.getInPort() != OFPort.OFPP_LOCAL.getValue()) {
                        salMatch.setField(new MatchField(MatchType.IN_PORT,
                                OF13NodeConnectorCreator.createNodeConnector(
                                        ofMatch.getInPort(), node)));
                    }
                    if (ofMatch.getDataLayerSource() != null
                            && !NetUtils
                                    .isZeroMAC(ofMatch.getDataLayerSource())) {
                        byte srcMac[] = ofMatch.getDataLayerSource();
                        salMatch.setField(new MatchField(MatchType.DL_SRC,
                                srcMac.clone()));
                    }
                    if (ofMatch.getDataLayerDestination() != null
                            && !NetUtils.isZeroMAC(ofMatch
                                    .getDataLayerDestination())) {
                        byte dstMac[] = ofMatch.getDataLayerDestination();
                        salMatch.setField(new MatchField(MatchType.DL_DST,
                                dstMac.clone()));
                    }
                    if (ofMatch.fieldExists(OFOXMFieldType.ETH_TYPE)) {
                        salMatch.setField(new MatchField(MatchType.DL_TYPE,
                                ofMatch.getDataLayerType()));
                    }
                    short vlan = ofMatch.getDataLayerVirtualLan();
                    if (vlan != OFVlanId.OFPVID_NONE.getValue()) {
                        salMatch.setField(new MatchField(MatchType.DL_VLAN,
                                vlan));
                    }
                    if (ofMatch.fieldExists(OFOXMFieldType.VLAN_PCP)) {
                        salMatch.setField(MatchType.DL_VLAN_PR, ofMatch
                                .getDataLayerVirtualLanPriorityCodePoint());
                    }
                    if (ofMatch.getNetworkSource() != 0) {
                        salMatch.setField(MatchType.NW_SRC, NetUtils
                                .getInetAddress(ofMatch.getNetworkSource()),
                                NetUtils.getInetAddress(ofMatch.getNetworkSourceMask()));
                    }
                    if (ofMatch.getNetworkDestination() != 0) {
                        salMatch.setField(MatchType.NW_DST, NetUtils
                                .getInetAddress(ofMatch.getNetworkDestination()),
                                NetUtils.getInetAddress(ofMatch.getNetworkDestinationMask()));
                    }
                    if (ofMatch.fieldExists(OFOXMFieldType.IP_DSCP)) {
                        salMatch.setField(MatchType.NW_TOS,
                                NetUtils.getUnsignedByte(ofMatch.getNetworkTypeOfService()));
                    }
                    if (ofMatch.fieldExists(OFOXMFieldType.IP_PROTO)) {
                        salMatch.setField(MatchType.NW_PROTO,
                                ofMatch.getNetworkProtocol());
                    }
                    if (ofMatch.fieldExists(OFOXMFieldType.TCP_SRC) ||
                        ofMatch.fieldExists(OFOXMFieldType.UDP_SRC) ||
                        ofMatch.fieldExists(OFOXMFieldType.SCTP_SRC)) {
                        salMatch.setField(MatchType.TP_SRC,
                                ofMatch.getTransportSource());
                    }
                    if (ofMatch.fieldExists(OFOXMFieldType.TCP_DST) ||
                            ofMatch.fieldExists(OFOXMFieldType.UDP_DST) ||
                            ofMatch.fieldExists(OFOXMFieldType.SCTP_DST)) {
                            salMatch.setField(MatchType.TP_DST,
                                    ofMatch.getTransportDestination());
                        }
                } else {
                    /* TODO: IPv6 support
                    V6Match v6Match = (V6Match) ofMatch;
                    if (v6Match.getInputPort() != 0 && v6Match.getInputPort() != OFPort.OFPP_LOCAL.getValue()) {
                        // Mask on input port is not defined
                        salMatch.setField(new MatchField(MatchType.IN_PORT,
                                OF13NodeConnectorCreator.createOFNodeConnector(
                                        v6Match.getInputPort(), node)));
                    }
                    if (v6Match.getDataLayerSource() != null
                            && !NetUtils
                                    .isZeroMAC(ofMatch.getDataLayerSource())) {
                        byte srcMac[] = v6Match.getDataLayerSource();
                        salMatch.setField(new MatchField(MatchType.DL_SRC,
                                srcMac.clone()));
                    }
                    if (v6Match.getDataLayerDestination() != null
                            && !NetUtils.isZeroMAC(ofMatch
                                    .getDataLayerDestination())) {
                        byte dstMac[] = v6Match.getDataLayerDestination();
                        salMatch.setField(new MatchField(MatchType.DL_DST,
                                dstMac.clone()));
                    }
                    if (v6Match.getDataLayerType() != 0) {
                        salMatch.setField(new MatchField(MatchType.DL_TYPE,
                                v6Match.getDataLayerType()));
                    }
                    short vlan = v6Match.getDataLayerVirtualLan();
                    if (vlan != 0) {
                        if (vlan == OFP_VLAN_NONE) {
                            vlan = MatchType.DL_VLAN_NONE;
                        }
                        salMatch.setField(new MatchField(MatchType.DL_VLAN,
                                vlan));
                    }
                    if ((v6Match.getWildcards() & OFMatch.OFPFW_DL_VLAN_PCP) == 0) {
                        salMatch.setField(MatchType.DL_VLAN_PR, v6Match
                                .getDataLayerVirtualLanPriorityCodePoint());
                    }
                    // V6Match may carry IPv4 address
                    if (v6Match.getNetworkSrc() != null) {
                        salMatch.setField(MatchType.NW_SRC,
                                v6Match.getNetworkSrc(),
                                v6Match.getNetworkSourceMask());
                    } else if (v6Match.getNetworkSource() != 0) {
                        salMatch.setField(MatchType.NW_SRC, NetUtils
                                .getInetAddress(v6Match.getNetworkSource()),
                                NetUtils.getInetNetworkMask(
                                        v6Match.getNetworkSourceMaskLen(),
                                        false));
                    }
                    // V6Match may carry IPv4 address
                    if (v6Match.getNetworkDest() != null) {
                        salMatch.setField(MatchType.NW_DST,
                                v6Match.getNetworkDest(),
                                v6Match.getNetworkDestinationMask());
                    } else if (v6Match.getNetworkDestination() != 0) {
                        salMatch.setField(MatchType.NW_DST,
                                NetUtils.getInetAddress(v6Match
                                        .getNetworkDestination()),
                                NetUtils.getInetNetworkMask(
                                        v6Match.getNetworkDestinationMaskLen(),
                                        false));
                    }
                    if (v6Match.getNetworkTypeOfService() != 0) {
                        int dscp = NetUtils.getUnsignedByte(v6Match
                                .getNetworkTypeOfService());
                        byte tos = (byte) (dscp >> 2);
                        salMatch.setField(MatchType.NW_TOS, tos);
                    }
                    if (v6Match.getNetworkProtocol() != 0) {
                        salMatch.setField(MatchType.NW_PROTO,
                                v6Match.getNetworkProtocol());
                    }
                    if (v6Match.getTransportSource() != 0) {
                        salMatch.setField(MatchType.TP_SRC,
                                (v6Match.getTransportSource()));
                    }
                    if (v6Match.getTransportDestination() != 0) {
                        salMatch.setField(MatchType.TP_DST,
                                (v6Match.getTransportDestination()));
                    }
                    */
                }
            }

            // Convert actions
            Action salAction = null;

            if (actionsList == null) {
                salActionList.add(new Drop());
            } else {
                int queueId = OFQueue.OFPQ_ALL.getValue();
                int lastTag = 0;
                for (OFAction ofAction : actionsList) {
                    if (ofAction instanceof OFActionOutput) {
                        int ofPort = ((OFActionOutput) ofAction).getPort();
                        if (ofPort == OFPort.OFPP_CONTROLLER.getValue()) {
                            salAction = new Controller();
                        } else if (ofPort == OFPort.OFPP_IN_PORT.getValue()) {
                            salAction = new Loopback();
                        } else if (ofPort == OFPort.OFPP_FLOOD.getValue()) {
                            salAction = new Flood();
                        } else if (ofPort == OFPort.OFPP_ALL.getValue()) {
                            salAction = new FloodAll();
                        } else if (ofPort == OFPort.OFPP_LOCAL.getValue()) {
                            salAction = new SwPath();
                        } else if (ofPort == OFPort.OFPP_NORMAL.getValue()) {
                            salAction = new HwPath();
                        } else if (ofPort == OFPort.OFPP_TABLE.getValue()) {
                            salAction = new HwPath(); // TODO: we do not handle
                                                      // table in sal for now
                        } else {
                            salAction = new Output(
                                    OF13NodeConnectorCreator.createOFNodeConnector(
                                            ofPort, node));
                        }

                        //If queueId was a previous action in the chain, then add Enqueue action
                        if (queueId != OFQueue.OFPQ_ALL.getValue()) {
                            salAction = new Enqueue(OF13NodeConnectorCreator.createOFNodeConnector(
                                ofPort, node),
                                ((OFActionSetQueue) ofAction).getQueueId());

                            // Reset after assigning
                            queueId = OFQueue.OFPQ_ALL.getValue();
                        }

                    } else if (ofAction instanceof OFActionSetQueue) {
                        queueId = ((OFActionSetQueue) ofAction).getQueueId();
                    } else if (ofAction instanceof OFActionSetField) {
                        OFOXMField field = ((OFActionSetField)ofAction).getField();
                        switch (field.getType()) {
                            case VLAN_VID:
                                if (lastTag == 0)
                                    salAction = new SetVlanId(((Short)field.getValue()).intValue() & 0xFFFF);
                                else {
                                    salAction = new PushVlan(lastTag, 0, 0, ((Short)field.getValue()).intValue() & 0xFFFF);
                                    lastTag = 0;
                                }
                                break;
                            case VLAN_PCP:
                                salAction = new SetVlanPcp((Byte)field.getValue());
                                break;
                            case ETH_SRC:
                                salAction = new SetDlSrc(((byte[])field.getValue()).clone());
                                break;
                            case ETH_DST:
                                salAction = new SetDlDst(((byte[])field.getValue()).clone());
                                break;
                            case IPV4_SRC:
                                byte addr[] = BigInteger.valueOf((Integer)field.getValue()).toByteArray();
                                InetAddress ip = null;
                                try {
                                    ip = InetAddress.getByAddress(addr);
                                } catch (UnknownHostException e) {
                                    logger.error("", e);
                                }
                                salAction = new SetNwSrc(ip);
                                break;
                            case IPV4_DST:
                                addr = BigInteger.valueOf((Integer)field.getValue()).toByteArray();
                                ip = null;
                                try {
                                    ip = InetAddress.getByAddress(addr);
                                } catch (UnknownHostException e) {
                                    logger.error("", e);
                                }
                                salAction = new SetNwDst(ip);
                                break;
                            case IP_DSCP:
                                byte dscp = (Byte)field.getValue();
                                salAction = new SetNwTos(dscp << 2);
                                //TODO: Support for ECN copying
                                break;
                            case TCP_SRC:
                                salAction = new SetTpSrc((Short)field.getValue());
                                break;
                            case TCP_DST:
                                salAction = new SetTpDst((Short)field.getValue());
                                break;
                            case UDP_SRC:
                                salAction = new SetTpSrc((Short)field.getValue());
                                break;
                            case UDP_DST:
                                salAction = new SetTpDst((Short)field.getValue());
                                break;
                            default: //Unknown field
                                break;
                        }

                    } else if (ofAction instanceof OFActionPopVLAN) {
                        salAction = new PopVlan();

                    }else if (ofAction instanceof OFActionPushVLAN) {
                        lastTag = ((Short)((OFActionPushVLAN)ofAction).getEtherType()).intValue() & 0xFFFF;
                    }
                    salActionList.add(salAction);
                }
            }
            // Create Flow
            flow = new Flow(salMatch, salActionList);
        }
        logger.trace("Openflow Match: {} Openflow Actions: {}", ofMatch,
                actionsList);
        logger.trace("SAL Flow: {}", flow);
        return flow;
    }

    private static final Map<Integer, Class<? extends Action>> actionMap = new HashMap<Integer, Class<? extends Action>>() {
        private static final long serialVersionUID = 1L;
        {
            put(1 << 0, Output.class);
            put(1 << 1, SetVlanId.class);
            put(1 << 2, SetVlanPcp.class);
            put(1 << 3, PopVlan.class);
            put(1 << 4, PushVlan.class);
            put(1 << 5, SetDlSrc.class);
            put(1 << 6, SetDlDst.class);
            put(1 << 7, SetNwSrc.class);
            put(1 << 8, SetNwDst.class);
            put(1 << 9, SetNwTos.class);
            put(1 << 10, SetTpSrc.class);
            put(1 << 11, SetTpDst.class);
            put(1 << 12, Enqueue.class);

        }
    };

    /**
     * Returns the supported flow actions for the netwrok node given the bitmask
     * representing the actions the Openflow 1.0 switch supports
     *
     * @param ofActionBitmask
     *            OF 1.0 action bitmask
     * @return The correspondent list of SAL Action classes
     */
    public static List<Class<? extends Action>> getFlowActions(int ofActionBitmask) {
        List<Class<? extends Action>> list = new ArrayList<Class<? extends Action>>();

        for (int i = 0; i < Integer.SIZE; i++) {
            int index = 1 << i;
            if ((index & ofActionBitmask) > 0) {
                if (actionMap.containsKey(index)) {
                    list.add(actionMap.get(index));
                }
            }
        }
        // Add implicit SAL actions
        list.add(Controller.class);
        list.add(SwPath.class);
        list.add(HwPath.class);
        list.add(Drop.class);
        list.add(Flood.class);
        list.add(FloodAll.class);

        return list;
    }

}
